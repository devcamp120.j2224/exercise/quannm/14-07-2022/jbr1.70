public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Time time1 = new Time(23, 59, 59);
        Time time2 = new Time(0, 0, 0);
        time1.display();
        time2.display();

        time1.nextSecond();
        time2.previousSecond();
        time1.display();
        time2.display();
    }
}